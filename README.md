# RAVIMOSHARK - LIBRARIES - REPORTS - JASPER OLD

This project contains *.jrxml* files to create reports using [Jasper Report Tool](https://community.jaspersoft.com/project/jasperreports-library).

> The **main repository** is hosted in [gitlab.com/ravimosharksas](https://gitlab.com/ravimosharksas/libs/reports/jasper-old.git) but it is automatically mirrored to [github.com/RavimoShark](https://github.com/RavimoShark/libs-reports-jasper-old.git), [gitlab.com/singletonsd](https://gitlab.com/singletonsd/ravimosharksas/libs-reports-jasper-old.git), [github.com/singletonsd](https://github.com/singletonsd/ravimoshark-libs-reports-jasper-old.git), [github.com/patoperpetua](https://github.com/patoperpetua/ravimoshark-libs-reports-jasper-old.git) and to [gitlab.com/patoperpetua](https://gitlab.com/patoperpetua/ravimoshark-libs-reports-jasper-old.git). If you are in the Github page it may occur that is not updated to the last version.

## HOW TO USE

### Connect to Prod DB

To connect to prod DB you need to create an .env file with the following variables:

```env
APP_DB_OLD_USERNAME=
APP_DB_OLD_PASSWORD=
```

After that, execute the script *./scripts/add_credentials_to_prod_data_adapter.sh*

Remember not to commit this file.

## DOCUMENTATION

- [Jasper Report Tool](https://community.jaspersoft.com/project/jasperreports-library)

## TODO

- [ ] Fix documentation.
- [ ] Add ci/cd.
- [ ] Make test.

----------------------

© [Singleton SD](http://www.singletonsd.com), France, 2019.
